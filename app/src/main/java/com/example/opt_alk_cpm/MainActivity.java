package com.example.opt_alk_cpm;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        navigateToFirstTable();
        navigateToSecondTable();
    }

    public void navigateToFirstTable() {
        Button button_easy = findViewById(R.id.navigateTable1);
        button_easy.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, TableActivity.class);
            startActivity(intent);
        });
    }

    public void navigateToSecondTable() {
        Button button_easy = findViewById(R.id.navigateTable2);
        button_easy.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, TableActivity2.class);
            startActivity(intent);
        });
    }
}