package com.example.opt_alk_cpm;

import android.graphics.Color;
import android.graphics.Paint;

import java.util.List;

public class Point {
    float x, y;
    List<Point> preDecessors;
    int time;
    String name;
    Paint color;

    Point(float x, float y, List<Point> preDec, int time, String name) {
        this.x = x;
        this.y = y;
        preDecessors = preDec;
        this.time = time;
        this.name = name;
        this.color = new Paint();
        this.color.setColor(Color.WHITE);
        this.color.setStrokeWidth(5f);
    }

    public void setColor(Paint color) {
        this.color = color;
    }
}