package com.example.opt_alk_cpm;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class GraphActivity extends AppCompatActivity {
    public List<Point> points;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new MyView(this));
        init();
    }

    // a c f
    // a b g
    public void init() {
        Paint selected = new Paint();
        selected.setColor(Color.YELLOW);

        Point A = new Point(100, 100, null, 2, "A");
        Point B = new Point(100, 500, Arrays.asList(A), 3, "B");
        Point C = new Point(550, 100, Arrays.asList(A), 4, "C");
        Point D = new Point(1000, 500, null, 5, "D");
        Point E = new Point(550, 500, Arrays.asList(B), 7, "E");
        Point F = new Point(1000, 100, Arrays.asList(C), 8, "F");
        Point G = new Point(550, 500, Arrays.asList(B), 9, "G");
        Point H = new Point(1000, 900, Arrays.asList(D), 1, "H");
        H.setColor(selected);
        points = new ArrayList<>(Arrays.asList(A, B, C, D, E, F, G, H));

    }


    public class MyView extends View {
        public MyView(Context context) {
            super(context);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            Paint paint = new Paint();
            paint.setColor(Color.WHITE);
            paint.setStrokeWidth(3);
            paint.setStyle(Paint.Style.STROKE);

            Path path = new Path();

            Paint node = new Paint();
            Paint text = new Paint();
            float offsetY = 25f;
            node.setColor(Color.RED);
            node.setStrokeWidth(2f);
            text.setColor(Color.BLACK);
            text.setTextSize(75f);
            text.setTextAlign(Paint.Align.CENTER);


            Paint circle = new Paint();
            circle.setColor(Color.RED);
            for (Point point : points) {
                path.moveTo(point.x, point.y);
                if (point.preDecessors != null) {
                    if (point.preDecessors.size() > 0) {
                        for (Point preDec : point.preDecessors) {
                            path.lineTo(preDec.x, preDec.y);
                            canvas.drawPath(path, paint);
                            Vector2D offset = calculateOffset(point.x, point.y, preDec.x, preDec.y);
                            drawRhombus(canvas, circle, point.x - offset.x, point.y - offset.y, 55);
                        }
                    }
                }
            }

            for (Point point : points) {
                canvas.drawCircle(point.x, point.y, 55f, point.color);
                canvas.drawText(point.name, point.x, point.y + offsetY, text);
            }
            canvas.drawPath(path, paint);

            Paint paintColor = new Paint();
            paintColor.setColor(Color.RED);
            Paint paintColor2 = new Paint();
            paintColor2.setColor(Color.BLUE);
            drawColoredLine(canvas, 100, 100, 550, 100, paintColor);
            drawColoredLine(canvas, 100, 100, 100, 500, paintColor2);
            drawColoredLine(canvas, 100, 500, 550, 500, paintColor2);
            drawColoredLine(canvas, 550, 100, 1000, 100, paintColor);
            drawColoredLine(canvas, 550, 100, 1000, 100, paintColor);
            drawTextOnSurface(canvas, 50, 1200, 55, "Kritikus út:  A -> B -> C -> F");
            drawTextOnSurface(canvas, 50, 1300, 55, "Az első kritikus út piros színnel,");
            drawTextOnSurface(canvas, 50, 1400, 55, "A második kék színnel van.");
        }
    }

    public void drawTextOnSurface(Canvas canvas, float posX, float posY, int textSize, String text) {
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.WHITE);
        paint.setTextSize(textSize);
        canvas.drawText(text, posX, posY, paint);
    }

    public void drawRhombus(Canvas canvas, Paint paint, float x, float y, int width) {
        int halfWidth = width / 2;

        Path path = new Path();
        path.moveTo(x, y + halfWidth);
        path.lineTo(x - halfWidth, y);
        path.lineTo(x, y - halfWidth);
        path.lineTo(x + halfWidth, y);
        path.lineTo(x, y + halfWidth);
        path.close();

        canvas.drawPath(path, paint);
    }

    public Vector2D calculateOffset(float x, float y, float xx, float yy) {
        if (x < xx) {
            return new Vector2D(-50, 0);
        }
        if (y < yy) {
            return new Vector2D(0, -50);
        }
        if (x > xx) {
            return new Vector2D(50, 0);
        }
        if (y > yy) {
            return new Vector2D(0, 50);
        }
        return new Vector2D(0, 0);
    }

    public void drawColoredLine(Canvas canvas, float begX, float begY, float endX, float endY, Paint paint) {
        paint.setColor(paint.getColor());
        paint.setStrokeWidth(6);
        paint.setStyle(Paint.Style.STROKE);

        Path path = new Path();
        path.moveTo(begX, begY);
        path.lineTo(endX, endY);
        canvas.drawPath(path, paint);
    }

}