package com.example.opt_alk_cpm;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class TableActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table);
        navigateToFirstGraph();
    }


    public void navigateToFirstGraph() {
        Button button_easy = findViewById(R.id.navigateToGraph1);
        button_easy.setOnClickListener(view -> {
            Intent intent = new Intent(TableActivity.this, GraphActivity.class);
            startActivity(intent);
        });
    }
}